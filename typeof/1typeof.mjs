//for primitive data type
console.log(typeof "a")//string
console.log(typeof 1)//number
console.log(typeof null)//boolean
let a=1
console.log(typeof a)

//for all non primitive , type is always object

console.log(typeof[1,3])
console.log(typeof{name:"nitan"})