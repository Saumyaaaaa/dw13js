let product=[
    {
        name:'laptop',
        price:10000,
    },
    {
        name:'mobile',
        price:50000,
    },
    {
        name:'tv',
        price:80000,
    },

]
//["laptop","mobile","tv"]

let arrayOfName= product.map((value,i)=>{
    return value.name
})
console.log(arrayOfName);


// [ 1000, 70000, 80000 ]
let arrayOfPrice= product.map((value,i)=>{
    return value.price
})
console.log(arrayOfPrice);

//[{name:"laptop",price:10000}]
let p1=product.filter((value,i)=>{
    if(value.price>10000){
        return true
    }
   
}
    )
    console.log(p1)