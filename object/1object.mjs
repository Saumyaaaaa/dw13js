//let info=["nitan",30,"false"]

//array is a collection of value
//where object is a collection of key value pair
//key value pair are called property
let info ={
    name:"nitan",
    age:30,
    isMarried:false,
}
//get value
console.log(info)
console.log(info.name)
console.log(info.age)
console.log(info.isMarried)
//changed value
info.name="yarden"
info.age=45
info.isMarried=true     //{ name: 'yarden', age: 45, isMarried: true }
console.log(info)

//delete is Married field
delete info.isMarried
console.log(info)      //{ name: 'yarden', age: 45 }


