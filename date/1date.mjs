let date=new Date()//it give current date and time
console.log(date)
console.log(new Date().toLocaleString())
console.log(new Date().toLocaleDateString())
console.log(new Date().toLocaleTimeString())


//new Date() gives date in iso format
//yyyy-mm-dd T hh:mm:ss
//2018-2-2T10:12:13 => invalid
//2018-02-02T10:12:13 => valid 