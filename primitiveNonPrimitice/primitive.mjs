//primitive => number,boolean,undefined,null,string
//memory location
//if let is used memory is allocated for that
let a=1
let b=a
let c=1


//===
// in case of primitive == see value
console.log(a===b)//true
console.log(a===c)//true



//non primitive => array,object,date,error
//memory allocation
//before allocating memory it check whether the variable is copy of another variable
//if the variable  is copy or another ,it will share a memory

let ar1=[1,2,3]
let ar2=ar1
let ar3=[1,2,3]


//===
// in case of non primitive === see memory address
console.log(ar1===ar2)//true
console.log(ar1===ar3)//false

ar1.push(5)
console.log(ar1)//[1,2,3,5]
console.log(ar2)//[1,2,3,5]
console.log(ar3)//[1,2,3]