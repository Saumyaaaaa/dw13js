let ar1=["a","b","c"]
let ar2=[1,2,3]

//["a","b","c",1,2,3]
//spread operator are the wrapper opener

let ar=[3,4,...ar1,10,...ar2];//[3,4,"a","b","c",1,2,3]
console.log(ar)

let arr=[...ar1,...ar2]
console.log(arr)//[ 'a', 'b', 'c', 1, 2, 3 ]

let arr1=[...ar2,...ar1]
console.log(arr1)//[1,2,3,"a","b","c"]