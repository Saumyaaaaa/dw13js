let firstLetterCapital =(name)=>{
    let arrayName=name.split("")
    let outputArray=arrayName.map((value,i)=>{
        if(i===0){
            return value.toUpperCase()
        }else{
            return value.toLowerCase()
        }

    })
    let output = outputArray.join("");
    return output;

}
let _firstLetterCapital=firstLetterCapital("nitan")
console.log(_firstLetterCapital)

