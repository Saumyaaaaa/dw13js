/* 
Suppose you have an array of objects representing students' scores in different subjects. Each object has the following structure:

const scores = [
    { name: "Alice", math: 90, science: 85, history: 88 },
    { name: "Bob", math: 88, science: 92, history: 85 },
    { name: "Charlie", math: 95, science: 78, history: 90 }
];
Write a function that takes in this array of scores and returns an array containing objects with the following structure:

[
    { name: "Alice", totalScore: 263, averageScore: 87.67 },
    { name: "Bob", totalScore: 265, averageScore: 88.33 },
    { name: "Charlie", totalScore: 263, averageScore: 87.67 }
]
Each object in the output array should have the student's name, total score (sum of all subject scores), and average score (total score divided by the number of subjects, rounded to two decimal places). You should use reduce, filter, and map to achieve this.
 */
const scores = [
    { name: "Alice", math: 90, science: 85, history: 88 },
    { name: "Bob", math: 88, science: 92, history: 85 },
    { name: "Charlie", math: 95, science: 78, history: 90 }
];
let arrayOfScores=scores.map((value,i)=>{

    let totalScore=Object.values(value).slice(1).reduce((prev,curr)=> prev +curr,0);
    let numberOfSubjects=Object.keys(value).length -1
    let averageScore=totalScore/numberOfSubjects;
     return{name: value.name , totalScore : Math.round(totalScore),averageScore:(averageScore).toFixed(2)};
})
console.log(arrayOfScores)
