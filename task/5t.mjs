/* 

Write a function that takes in this array of employees and returns an object containing department-wise statistics. The object should have the following structure:


{
    HR: { totalEmployees: 2, averageSalary: 55000 },
    Engineering: { totalEmployees: 2, averageSalary: 77500 },
    Marketing: { totalEmployees: 2, averageSalary: 62500 }
}
Each property in the output object represents a department. For each department, you should calculate the total number of employees and the average salary of employees in that department. You should use reduce and other array methods as necessary to achieve this.
 */
const employees = [
    { id: 1, name: "Alice", department: "HR", salary: 50000 },
    { id: 2, name: "Bob", department: "Engineering", salary: 80000 },
    { id: 3, name: "Charlie", department: "HR", salary: 60000 },
    { id: 4, name: "David", department: "Marketing", salary: 70000 },
    { id: 5, name: "Emma", department: "Engineering", salary: 75000 },
    { id: 6, name: "Frank", department: "Marketing", salary: 55000 }
];
let departmentData = employees.reduce((acc, curr) => {
    let { department, salary } = curr;
    if (!acc[department]) {
        acc[department] = { totalEmployees: 0, totalSalary: 0 };
    }
    acc[department].totalEmployees++;
    acc[department].totalSalary += salary;
    return acc;
}, {});

// Calculate average salary for each department
Object.keys(departmentData).forEach(department => {
    departmentData[department].averageSalary = departmentData[department].totalSalary / departmentData[department].totalEmployees;
});

console.log(departmentData);


