/* 

const transactions = [
    { id: 1, amount: 100 },
    { id: 2, amount: 200 },
    { id: 3, amount: -50 },
    { id: 4, amount: 300 },
    { id: 5, amount: -150 }
];
Write a function that takes in this array of transactions and returns the total profit made by the company. The profit is calculated as the sum of all positive amounts in the transactions array. You should use reduce to achieve this.

Once you have the total profit function, here's an additional challenge:

Write another function that takes in the same array of transactions and returns an array of objects containing only the transactions with negative amounts (i.e., transactions representing expenses). You should use filter to achieve this.

 */

const transactions = [
    { id: 1, amount: 100 },
    { id: 2, amount: 200 },
    { id: 3, amount: -50 },
    { id: 4, amount: 300 },
    { id: 5, amount: -150 }
];
let 
totalProfit=transactions.reduce((prev,curr)=>{
    if(curr.amount>0) {
    return prev+curr.amount
}
else{
    return prev
}
},0)


console.log("Total Profit:",totalProfit)

let arrayOfNegative=transactions.filter((value,index)=>{
    return value.amount<0;

})
console.log("Expense:",arrayOfNegative)
